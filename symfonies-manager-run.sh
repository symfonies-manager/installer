#!/bin/bash

# set install directory
DIR="!DIRECTORY!"
LOG=$DIR/last_run
#LOG=/dev/null

# empty log
echo -e "Last run: $(date)\n" > $LOG

# change to tmp dir
cd /tmp || exit 1

# start symfony proxy
symfony proxy:start >> $LOG

# kill current applications
fuser -k 3999/tcp >> $LOG
fuser -k 4299/tcp >> $LOG

# run applications
nohup npm run start --prefix $DIR/api >> $LOG 2>&1 &
nohup npm run start --prefix $DIR/site >> $LOG 2>&1 &
