#!/bin/bash

#DIR=/tmp/.symfonies-manager
DIR="${HOME}/.symfonies-manager"

# define colors used
NOCOLOR='\033[0m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'

# directory creation
rm -rf $DIR
mkdir $DIR
cd $DIR || exit 1

# install api server
git clone https://gitlab.com/symfonies-manager/api.git
cd api || exit 1
npm install
cd ..

# install site client
git clone https://gitlab.com/symfonies-manager/site.git
cd site || exit 1
npm install
cd ..

# get running script and edit that to match the current install directory
rm -f symfonies-manager-run.sh
wget https://gitlab.com/symfonies-manager/installer/raw/master/symfonies-manager-run.sh
cat symfonies-manager-run.sh |sed "s/!DIRECTORY!/$(echo $DIR | sed 's_/_\\/_g')/g" > symfonies-manager-run.sh
chmod +x symfonies-manager-run.sh

# execute run now
./symfonies-manager-run.sh

# output final messages
echo
echo -e "${GREEN}INSTALLATION COMPLETED!${NOCOLOR}"
echo "Now put in your crontab these lines:"
echo -e "\t${CYAN}@reboot sleep 10 && $DIR/symfonies-manager-run.sh >/dev/null 2>&1"
echo -e $NOCOLOR
echo "Symfony manager is now running at this url: http://localhost:4299/"
echo -e $NOCOLOR
