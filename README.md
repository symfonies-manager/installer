# Symfonies manager installer

## Installation
Just run this command:

```bash
wget https://gitlab.com/symfonies-manager/installer/raw/master/symfonies-manager-installer.sh -O - | bash
```
